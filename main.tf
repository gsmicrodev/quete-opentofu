# Importation des modules
module "lxc_container" {
  source = "./modules/lxc_container"

  # Passer les valeurs des variables appropriées aux modules
  proxmox_node     = "wcs-cyber-node01"
  pve_api_token    = "terraform-prov@pve!terraform=d24473f3-db7a-4e9f-9a9c-b857ad093078"
  pve_host_address = "https://wcs-cyber-node01.v6.rocks:8006/"
  pve_ssh_user     = "root"
  tmp_dir          = "/tmp"

  ct_id                          = "143"
  network_name                   = "eth0"
  ct_network_bridge              = "vmbr2"
  ct_datastore_storage_location  = "local-nvme-datas"
  ct_datastore_template_location = "local-hdd-templates"
  ct_disk_size                   = 8
  ct_memory                      = 1024
  ct_source_file_path            = "local-hdd-templates:/mnt/hdd-templates/template/cache/debian-12-standard_12_amd64.tar.zst"
  ct_hostname                    = "opentofuTofu"
  dns_domain                     = "opentofuTofu"
  dns_servers                    = []
  gateway                        = "192.168.1.254"
  os_type                        = "debian"
}


#module "firewall_rule" {
#source = "./modules/firewall_rule"

# Passer les valeurs des variables appropriées aux modules
# allowed_ports = var.allowed_ports
#}
