resource "random_password" "container_root_password" {
  length = 24
  override_special = "_%@"
  special          = true
}

resource "tls_private_key" "debian_container_key" {
  algorithm = "ED25519"
  rsa_bits  = 4096
}

# Définition de la ressource pour créer un conteneur LXC avec le fournisseur bpg/proxmox
resource "proxmox_virtual_environment_container" "debian_container" {
  description = "Managed by OpenTofu"
  node_name = var.proxmox_node
  start_on_boot = true
  tags = ["debian12", "demobis"]
  unprivileged = true
  vm_id = var.ct_id 

  cpu {
    architecture = "amd64"
    cores = 2
  }

  disk {
    datastore_id = var.ct_datastore_storage_location
    size = var.ct_disk_size
  }

  memory {
    dedicated = var.ct_memory
    swap = 0
  }

  operating_system {
    template_file_id = var.ct_source_file_path
    type             = var.ct_type
  }

  initialization {
    hostname = var.ct_hostname

    dns {
      domain = var.dns_domain
      servers = var.dns_servers

    }

    ip_config {
      ipv4 {
        address = "192.168.1.200/24"
        gateway = var.gateway
      }
    }

    user_account {
      keys = [
        trimspace(tls_private_key.debian_container_key.public_key_openssh)
      ]
      password = random_password.container_root_password.result
    }
  }

  network_interface {
    name = var.network_name
    bridge = var.ct_network_bridge
  }

  features {
    nesting = true
    fuse = false
    }
}

