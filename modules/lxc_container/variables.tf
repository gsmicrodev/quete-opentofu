variable "datastore_id" {
  type = string
  default = ""
}

variable "proxmox_node" {
  type        = string
  default = ""
}

variable "pve_api_token" {
  type        = string
  default = ""
}

variable "pve_host_address" {
  type        = string
  default = ""
}

variable "tmp_dir" {
  type        = string
    default = "/tmp"
}

variable "network_name" {
  type        = string
  default = "eth0"
}

variable "ct_network_bridge" {
  type        = string
  default = "vmbr0"
}

variable "ct_id" {
  type        = string
  default = 9999
}

variable "ct_datastore_storage_location" {
  type        = string
  default = ""
}

variable "ct_datastore_template_location" {
  type = string
  default = "" 
}

variable "ct_disk_size" {
  type        = string
  default = ""
}

variable "ct_source_file_path" { 
  type        = string
  default = ""
}

variable "ct_hostname" { 
  type        = string
  default = ""
}

variable "ct_memory" {
  type        = string
  default = ""
}

variable "ct_type" {
  type        = string
  default = "debian"
}

variable "dns_domain" { 
  type        = string
  default = ""
}

variable "dns_servers" {
  type        = list(string)
  default = []
}

variable "gateway" { 
  type        = string
  default = "192.168.1.254"
}

variable "os_type" {
  type        = string
  default = "debian"
}

variable "pve_ssh_user" {
  type        = string
  default = ""
}



