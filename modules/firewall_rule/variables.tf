variable "allowed_ports" {
  description = "List of allowed ports"
  type        = list(number)
  default = [80, 443, 22]
}
variable "pve_api_token" {
  type        = string
  default = ""
}

variable "pve_host_address" {
  type        = string
  default = ""
}

variable "tmp_dir" {
  type        = string
    default = "/tmp"
}

variable "pve_ssh_user" {
  type        = string
  default = ""
}