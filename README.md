OpenTofu
OpenTofu est un projet Terraform modulaire pour provisionner une architecture basique mais complète sur Proxmox, incluant des conteneurs LXC sous Debian 12 et des règles de sécurité.

Objectif
Le but de ce projet est de fournir une infrastructure flexible et réutilisable pour le déploiement automatisé de conteneurs LXC sur Proxmox, tout en assurant la sécurité grâce à des règles de pare-feu.

Utilisation
Cloner ce dépôt sur votre machine locale :

```bash
git clone https://gitlab.com/gsmicrodev/quete-opentofu.git
```

Personnaliser les variables dans le fichier conf.tfvars selon vos besoins.

Exécuter Terraform pour déployer l'infrastructure :

```bash
tofu init
tofu plan -var-file=conf.tfvars
tofu apply -var-file=conf.tfvars
```

Suivre les instructions affichées par Terraform pour confirmer le déploiement.

Dépendances
Ce projet dépend de Terraform pour la gestion de l'infrastructure. Assurez-vous d'avoir Terraform installé localement avant d'exécuter les commandes.

Structure du Projet
main.tf: Définition de la configuration principale de l'infrastructure.
variables.tf: Définition des variables utilisées dans le projet.
conf.tfvars: Fichier de variables contenant les valeurs spécifiques à votre environnement.
modules/: Répertoire contenant les modules Terraform pour gérer différentes parties de l'infrastructure.
.gitignore: Fichier spécifiant les éléments à ignorer lors du suivi avec Git.
README.md: Ce fichier, fournissant des instructions et des informations sur le projet.


   